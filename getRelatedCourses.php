<?php
 header("Content-Type: application/json; charset=UTF-8");
 header('Access-Control-Allow-Origin: http://localhost:3000');
 header('Access-Control-Allow-Methods: GET, POST,DELETE,PATCH');
 header("Access-Control-Allow-Credentials: true");
 header("Access-Control-Allow-Headers: Content-Type");
    include_once 'course.php';
    include_once 'connection.php';
  
    if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    $database = new Database();
    $db = $database->getConnection();
    $items = new Course($db);
    $cid = isset($_GET['cid']) ? $_GET['cid'] : die();

    @$stmt = $items->getAllCoursesRelated($cid);
    $itemCount = $stmt->rowCount();

    if($itemCount > 0){
        $courseArray = array();
        $courseArray["body"] = array();
        $courseArray["itemCount"] = $itemCount;
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
            extract($row);
            $e = array(
                "id" => $id,
                "name" => $name,
                "isActive" => $isActive,
                "teacher_id" => $teacher_id,
                "lab_grade_per" => $lab_grade_per,
                "theory_grade_per" => $theory_grade_per,
                "semester" => $semester,
                "year" => $year,
                "note" => $note,
                "Created" => $Created
            );
            array_push($courseArray["body"], $e);
        }
        echo json_encode($courseArray);
    }
    else{
        http_response_code(404);
        echo json_encode(
            array("message" => "No record found.")
        );
    }
}
else{
    http_response_code(400);
    echo json_encode("INVALID METHOD");
}
?>