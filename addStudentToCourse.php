<?php
 header("Content-Type: application/json; charset=UTF-8");
 header('Access-Control-Allow-Origin: http://localhost:3000');
 header('Access-Control-Allow-Methods: GET, POST,DELETE,PATCH');
 header("Access-Control-Allow-Credentials: true");
 header("Access-Control-Allow-Headers: Content-Type");
 header("HTTP/1.1 200 OK");
    
   include_once 'person.php'; 
   include_once 'course.php'; 
   include_once 'grade.php'; 
   include_once 'connection.php';
  
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $database = new Database();
        $db = $database->getConnection();
        $std = new Person($db);
        $course = new Course($db);
        $grades = new Grade($db);
     

        $data = json_decode(file_get_contents("php://input"));
        $std->id = $data->student_id;
        @$std->getSingleEmployee();
        if ($std->name === null) {
            http_response_code(400);
            echo json_encode("Cannot create, student does not exist in database");
            return false;
        }

        $course->id = $data->course_id;
        @$course->getCourse();
        if ($course->name === null) {
            http_response_code(400);
            echo json_encode("Cannot create, course does not exist in database");
            return false;
        }

        @$isEnrolled = $std->checkIfStudentIsEnrolled($data->student_id,$data->course_id);
        @$isEnrolled = $isEnrolled->fetch(PDO::FETCH_ASSOC);

    

        if ($isEnrolled != false) {
            http_response_code(400);
            echo json_encode("Cannot add, student already enrolled");
            return false;
        }

        @$checkIfPassedAllRelatedCourses = $grades->getAllNagativeGradesFromRelatedCourses($data->student_id,$data->course_id);
        @$itemCount = $checkIfPassedAllRelatedCourses->rowCount();
        
        if ($itemCount != 0) {
            http_response_code(400);
            echo json_encode("Cannot add, student has not passed one or more related courses");
            return false;
        }

        
        if(@$course->addStudentToCourse($data->student_id,$data->course_id,$data->note)){
            echo json_encode("Enrolled successfully.");
        } else{
            http_response_code(400);
            echo json_encode("Cannot enroll.");
        }
   }
   else{
    http_response_code(400);
    echo json_encode("INVALID METHOD");
}
  
    
    ?>