<?php
  header("Content-Type: application/json; charset=UTF-8");
  header('Access-Control-Allow-Origin: http://localhost:3000');
  header('Access-Control-Allow-Methods: GET, POST,DELETE,PATCH');
  header("Access-Control-Allow-Credentials: true");
  header("Access-Control-Allow-Headers: Content-Type");
    include_once 'course.php'; 
    include_once 'connection.php';
    if ($_SERVER['REQUEST_METHOD'] === 'POST') { 
    
        $database = new Database();
        $db = $database->getConnection();
        $item = new Course($db);
        $item->id = isset($_GET['id']) ? $_GET['id'] : die();

        $courseIfPersist = new Course($db);
        $courseIfPersist->id = $item->id;
        $courseIfPersist->getCourse();

        if ($courseIfPersist->teacher_id===0){
            http_response_code(400);
            echo json_encode("Cannot unbind, teacher has already been unbined");
            return false;
        }

        if($courseIfPersist->name != null  && $item->unBindTeacherFromCourse($item->id)){
            echo json_encode("Teacher unbined.");
        } else{
            http_response_code(404);
            echo json_encode("Course does not exist");
        }
    }   else{
        http_response_code(400);
        echo json_encode("INVALID METHOD");
    }

?>