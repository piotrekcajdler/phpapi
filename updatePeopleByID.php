  <?php
  header("Content-Type: application/json; charset=UTF-8");
  header('Access-Control-Allow-Origin: http://localhost:3000');
  header("HTTP/1.1 200 OK");
  header("Access-Control-Allow-Credentials: true");
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');
    
   include_once 'person.php'; 
   include_once 'connection.php';
  
   if ($_SERVER['REQUEST_METHOD'] === 'PATCH') {
    $database = new Database();
    $db = $database->getConnection();
    $item = new Person($db);
    $data = json_decode(file_get_contents("php://input"));

    $nP = new Person($db);
    $nP->id = $data->id;
    $nP->getSingleEmployee();
    if ($nP-> name === null) {
        http_response_code(400);
        echo json_encode("Data could not be updated, User does not exist");
        return false;

    }


    $item->id = $data->id;
    $item->name= $data->name;
    $item->lastname=$data->lastname;
    $item->email = $data->email;
    $item->universityID = $data->universityID;
    $item->phoneNumber=$data->phoneNumber;
    $item->role = $data->role;
    $item->degree = $data->degree;
    $item->note = $data->note;

    if($item->updateEmployee()){
        echo json_encode("Employee data updated.");
    } else{
        http_response_code(400);
        echo json_encode("Data could not be updated");
    }}else{
        http_response_code(400);
        echo json_encode("INVALID METHOD");
    }
    
    ?>