<?php
    class Grade{
        // Connection
        private $conn;
        // Table
        private $db_table = "grade";
        // Columns
        public $id;
        public $grade;
        public $course_id;
        public $student_id;
        public $isSubmitted;
        public $isActive;
        public $note;
        public $type;
        public $Created;
        // Db connection
        public function __construct($db){
            $this->conn = $db;
        }
        // GET ALL
        public function getAllGrades(){
            $sqlQuery = "SELECT * FROM " . $this->db_table . " WHERE isActive=1";
            $stmt = $this->conn->prepare($sqlQuery);
            $stmt->execute();
            return $stmt;
        }

        public function getAllGradesFromCourse(&$cid){
            $sqlQuery = "SELECT * FROM " . $this->db_table . " WHERE isActive=1 AND course_id=:cid";
            $stmt = $this->conn->prepare($sqlQuery);
            $stmt->bindParam(":cid", htmlspecialchars(strip_tags($cid)));
            $stmt->execute();
            return $stmt;
        }

        public function getAllGradesForStudent(&$sid){
            $sqlQuery = "SELECT * FROM " . $this->db_table . " WHERE isActive=1 AND student_id=:sid";
            $stmt = $this->conn->prepare($sqlQuery);
            $stmt->bindParam(":sid", htmlspecialchars(strip_tags($sid)));
            $stmt->execute();
            return $stmt;
        }
        public function getAllGradesForStudentFromCourse(&$sid,&$cid){
            $sqlQuery = "SELECT * FROM " . $this->db_table . " WHERE isActive=1 AND student_id=:sid AND course_id=:cid";
            $stmt = $this->conn->prepare($sqlQuery);
            $stmt->bindParam(":sid", htmlspecialchars(strip_tags($sid)));
            $stmt->bindParam(":cid", htmlspecialchars(strip_tags($cid)));
            $stmt->execute();
            return $stmt;
        }
        

        // CREATE
        public function createGrade($isFinal){
              $sqlQuery = "";
            
            if ($isFinal === "0")
                $sqlQuery = "INSERT INTO
                        ". $this->db_table ."
                    SET
                        grade = :grade, 
                        course_id = :course_id,
                        student_id = :student_id, 
                        isSubmitted = false,
                        type = :type,
                        isActive = true, 
                        note = :note,
                        Created=NOW()
                        ";

            else $sqlQuery = "INSERT INTO
            ". $this->db_table ."
        SET
            grade = :grade, 
            course_id = :course_id,
            student_id = :student_id, 
            type = :type,
            isSubmitted = true,
            isActive = true, 
            note = :note,
            Created=NOW()
            ";
        
            $stmt = $this->conn->prepare($sqlQuery);
        
            // sanitize
            $this->grade=htmlspecialchars(strip_tags($this->grade));
            $this->course_id=htmlspecialchars(strip_tags($this->course_id));
            $this->student_id=htmlspecialchars(strip_tags($this->student_id));
            $this->note=htmlspecialchars(strip_tags($this->note));
            $this->type=htmlspecialchars(strip_tags($this->type));
        
            // bind data
            $stmt->bindParam(":grade", $this->grade);
            $stmt->bindParam(":course_id", $this->course_id);
            $stmt->bindParam(":student_id", $this->student_id);
            $stmt->bindParam(":note", $this->note);
            $stmt->bindParam(":type", $this->type);
        
            if($stmt->execute()){
               return true;
            }
            return false;
        }
        // READ single
        public function getGrade(){
            $sqlQuery = "SELECT * FROM " . $this->db_table . " WHERE isActive=1 AND id = ?";

            $stmt = $this->conn->prepare($sqlQuery);
            $stmt->bindParam(1, $this->id);
            $stmt->execute();
            $dataRow = $stmt->fetch(PDO::FETCH_ASSOC);
            if (!empty($dataRow['grade'])){
                $this->grade = $dataRow['grade'];
                $this->course_id = $dataRow['course_id'];
                $this->student_id = $dataRow['student_id'];
                $this->isActive = $dataRow['isActive'];
                $this->isSubmitted = $dataRow['isSubmitted'];
                $this->note = $dataRow['note'];
                $this->Created = $dataRow['Created'];}
        }    
        
        public function getFinalGradeForStudentFromCourse(&$sid,&$cid){
            $sqlQuery = "SELECT * FROM " . $this->db_table . " WHERE isActive=1 AND isSubmitted=1 AND type='final' AND student_id=:sid AND course_id=:cid";
            $stmt = $this->conn->prepare($sqlQuery);
            $stmt->bindParam(":sid", htmlspecialchars(strip_tags($sid)));
            $stmt->bindParam(":cid", htmlspecialchars(strip_tags($cid)));
            $stmt->execute();
            return $stmt;
        }
        // UPDATE
        public function updateGrade(){
            $sqlQuery = "UPDATE
                        ". $this->db_table ."
                    SET
                    grade = :grade,
                    note = :note
                    WHERE 
                        id = :id";
        
            $stmt = $this->conn->prepare($sqlQuery);
        
           // sanitize
           $this->id=htmlspecialchars(strip_tags($this->id));
           $this->grade=htmlspecialchars(strip_tags($this->grade));
           $this->note=htmlspecialchars(strip_tags($this->note));
       
           // bind data
           $stmt->bindParam(":id", $this->id);
           $stmt->bindParam(":grade", $this->grade);
           $stmt->bindParam(":note", $this->note);
        
            if($stmt->execute()){
               return true;
            }
            return false;
        }
        // DELETE
        function deleteGrade(){
            $sqlQuery = "UPDATE
            ". $this->db_table ."
        SET
            isActive = 0
        WHERE 
            id = ?";

            $stmt = $this->conn->prepare($sqlQuery);
            $this->id=htmlspecialchars(strip_tags($this->id));
            $stmt->bindParam(1, $this->id);
            if($stmt->execute()){
                return true;
            }
            return false;
        }

        //Submit
        function submitGrade(){
            $sqlQuery = "UPDATE
            ". $this->db_table ."
        SET
            isSubmitted = 1
        WHERE 
            id = :id AND isActive=1";

            $stmt = $this->conn->prepare($sqlQuery);
            $this->id=htmlspecialchars(strip_tags($this->id));
            $stmt->bindParam(":id", $this->id);
            if($stmt->execute()){
                return true;
            }
            return false;
        }

        

        public function getAllNagativeGradesFromRelatedCourses($sid,$cid){
            $sqlQuery = "SELECT * from grade where grade < 5 and student_id = :sid and isActive =1 and isSubmitted=1 and course_id IN (SELECT related_course_id from course_related where course_id = :cid);
";
            $stmt = $this->conn->prepare($sqlQuery);
            $stmt->bindParam(":sid", htmlspecialchars(strip_tags($sid)));
            $stmt->bindParam(":cid", htmlspecialchars(strip_tags($cid)));
            $stmt->execute();
            return $stmt;

    }
        
    }
?>
