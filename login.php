<?php
     header("Content-Type: application/json; charset=UTF-8");
     header('Access-Control-Allow-Origin: http://localhost:3000');
     header('Access-Control-Allow-Methods: GET, POST');
     header("Access-Control-Allow-Credentials: true");
     header("Access-Control-Allow-Headers: Content-Type"); 
     header("HTTP/1.1 200 OK");
    
   include_once 'person.php'; 
   include_once 'connection.php';
   if ($_SERVER['REQUEST_METHOD'] === 'POST') {

    $database = new Database();
    $db = $database->getConnection();
    $item = new Person($db);
    $data = json_decode(file_get_contents("php://input"));
    $item->email = $data->email;
    $item->password = $data->password;
    @$item->login();
    if($item->name != null){
        $e = array(
            "id" => $item->id,
            "name" => $item->name,
            "lastname" => $item->lastname,
            "isActive" => $item->isActive,
            "email" => $item->email,
            "universityID" => $item->universityID,
            "phoneNumber" => $item->phoneNumber,
            "role" => $item->role,
            "degree" => $item->degree,
            "note" => $item->note,
            "Created" => $item->Created
        );
      
        http_response_code(200);
        echo json_encode($e);
    }
    else{
        http_response_code(403);
        echo json_encode("WRONG EMAIL OR PASSWORD");
    }}else{
        http_response_code(400);
        echo json_encode("INVALID METHOD");
    }
?>

