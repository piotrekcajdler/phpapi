<?php
     header("Content-Type: application/json; charset=UTF-8");
     header('Access-Control-Allow-Origin: http://localhost:3000');
     header('Access-Control-Allow-Methods: GET, POST,DELETE,PATCH');
     header("Access-Control-Allow-Credentials: true");
     header("Access-Control-Allow-Headers: Content-Type");
     header("HTTP/1.1 200 OK");
    include_once 'grade.php'; 
    include_once 'connection.php';
    if ($_SERVER['REQUEST_METHOD'] === 'POST') { 
    
    $database = new Database();
     $db = $database->getConnection();
     $item = new Grade($db);
     $item->id = isset($_GET['id']) ? $_GET['id'] : die();
     $operation =  isset($_GET['op']) ? $_GET['op'] : die();

    $gradeIfPersist = new Grade($db);
    $gradeIfPersist->id = $item->id;
    $gradeIfPersist->getGrade();

    if($gradeIfPersist->grade != null && strcmp("sub",$operation) === 0 && $item->submitGrade()){
        echo json_encode("Grade Submitted.");
    } 
    else if($gradeIfPersist->grade != null && strcmp("del",$operation) === 0 && $item->deleteGrade()){
        echo json_encode("Grade deleted.");
    } 
    
    
    else{
        http_response_code(404);
        echo json_encode("Data could not be deleted");
        
    }}else{
        http_response_code(400);
        echo json_encode("INVALID METHOD");
    }

?>