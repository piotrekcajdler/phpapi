-- phpMyAdmin SQL Dump
-- version 5.1.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 15, 2022 at 10:13 PM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 7.4.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `university`
--

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `id` int(10) UNSIGNED NOT NULL,
  `teacher_id` int(10) UNSIGNED NOT NULL,
  `theory_grade_per` float NOT NULL,
  `lab_grade_per` float NOT NULL,
  `name` text NOT NULL,
  `semester` int(11) NOT NULL,
  `year` text NOT NULL,
  `note` text NOT NULL,
  `isActive` tinyint(1) NOT NULL,
  `Created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`id`, `teacher_id`, `theory_grade_per`, `lab_grade_per`, `name`, `semester`, `year`, `note`, `isActive`, `Created`) VALUES
(1, 2, 0.7, 2019, 'Polish', 2, '2019/2020', 'dupa', 1, '2022-04-15 17:53:22'),
(2, 3, 0.7, 2019, 'Maths', 4, '2019/2020', 'dupa', 1, '2022-04-15 17:59:13'),
(3, 5, 0.7, 0.3, 'Maths', 4, '2019/2020', 'dupa', 1, '2022-04-15 20:12:20');

-- --------------------------------------------------------

--
-- Table structure for table `course_related`
--

CREATE TABLE `course_related` (
  `course_id` int(10) UNSIGNED NOT NULL,
  `related_course_id` int(10) UNSIGNED NOT NULL,
  `note` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `course_related`
--

INSERT INTO `course_related` (`course_id`, `related_course_id`, `note`) VALUES
(2, 1, 'kupadupa');

-- --------------------------------------------------------

--
-- Table structure for table `grade`
--

CREATE TABLE `grade` (
  `id` int(11) NOT NULL,
  `student_id` int(10) UNSIGNED NOT NULL,
  `course_id` int(10) UNSIGNED NOT NULL,
  `grade` int(11) NOT NULL,
  `note` text NOT NULL,
  `type` text NOT NULL,
  `isActive` tinyint(1) NOT NULL,
  `isSubmitted` tinyint(1) NOT NULL,
  `Created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `grade`
--

INSERT INTO `grade` (`id`, `student_id`, `course_id`, `grade`, `note`, `type`, `isActive`, `isSubmitted`, `Created`) VALUES
(1, 1, 1, 2, 'kupa', '', 1, 0, '2022-04-15 17:55:46'),
(2, 1, 1, 2, 'kupa', '', 1, 0, '2022-04-15 17:55:49'),
(3, 1, 1, 6, 'kupa', '', 1, 1, '2022-04-15 17:56:02'),
(4, 1, 1, 2, 'kupa', '', 1, 1, '2022-04-15 17:56:14'),
(5, 1, 1, 2, 'kupa', '', 1, 1, '2022-04-15 17:56:19'),
(7, 1, 1, 6, 'kupa', 'final', 1, 1, '2022-04-15 19:39:05'),
(8, 4, 1, 2, 'kupa', 'final', 1, 1, '2022-04-15 19:51:01');

-- --------------------------------------------------------

--
-- Table structure for table `persons`
--

CREATE TABLE `persons` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` text NOT NULL,
  `lastname` text NOT NULL,
  `email` text NOT NULL,
  `password` text NOT NULL,
  `phoneNumber` text NOT NULL,
  `isActive` tinyint(1) NOT NULL,
  `role` text NOT NULL,
  `degree` text NOT NULL,
  `universityID` text NOT NULL,
  `note` text NOT NULL,
  `Created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `persons`
--

INSERT INTO `persons` (`id`, `name`, `lastname`, `email`, `password`, `phoneNumber`, `isActive`, `role`, `degree`, `universityID`, `note`, `Created`) VALUES
(1, 'Piotr', 'Cajdler', 'piotrek@o2.pl', 'P!OTREK!@#!', '1123', 1, 'student', '', 'alamaKOta', 'kocham bombla', '2022-04-15 17:49:28'),
(2, 'Piotr', 'Cajdler', 'piotrek1@o2.pl', 'P!OTREK!@#!', '1123', 1, 'teacher', '', 'alamaKOta', 'kocham bombla', '2022-04-15 17:50:05'),
(3, 'Piotr', 'Cajdler', 'piotrek2@o2.pl', 'P!OTREK!@#!', '1123', 1, 'teacher', '', 'alamaKOta', 'kocham bombla', '2022-04-15 17:59:01'),
(4, 'Piotr', 'Cajdler', 'piotrek3@o2.pl', 'P!OTREK!@#!', '1123', 1, 'student', '', 'alamaKOta', 'kocham bombla', '2022-04-15 19:50:41'),
(5, 'Piotr', 'Cajdler', 'piotrek4@o2.pl', 'P!OTREK!@#!', '1123', 1, 'teacher', '', 'alamaKOta', 'kocham bombla', '2022-04-15 20:11:41');

-- --------------------------------------------------------

--
-- Table structure for table `person_courses`
--

CREATE TABLE `person_courses` (
  `student_id` int(10) UNSIGNED NOT NULL,
  `course_id` int(10) UNSIGNED NOT NULL,
  `note` text NOT NULL,
  `Created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `person_courses`
--

INSERT INTO `person_courses` (`student_id`, `course_id`, `note`, `Created`) VALUES
(1, 1, 'kupadupa', '2022-04-15 19:33:38'),
(4, 1, 'kupadupa', '2022-04-15 19:50:52');

-- --------------------------------------------------------

--
-- Table structure for table `teacher_courses_history`
--

CREATE TABLE `teacher_courses_history` (
  `teacher_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `semester` int(11) NOT NULL,
  `year` text NOT NULL,
  `Created` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `teacher_courses_history`
--

INSERT INTO `teacher_courses_history` (`teacher_id`, `course_id`, `semester`, `year`, `Created`) VALUES
(2, 1, 2, '2019/2020', '2022-04-15'),
(2, 1, 2, '2019/2020', '2022-04-15'),
(5, 3, 4, '2019/2020', '2022-04-15');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `teacher_id` (`teacher_id`);

--
-- Indexes for table `course_related`
--
ALTER TABLE `course_related`
  ADD KEY `course_id` (`course_id`),
  ADD KEY `related_course_id` (`related_course_id`);

--
-- Indexes for table `grade`
--
ALTER TABLE `grade`
  ADD PRIMARY KEY (`id`),
  ADD KEY `student_id` (`student_id`),
  ADD KEY `course_id` (`course_id`);

--
-- Indexes for table `persons`
--
ALTER TABLE `persons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `person_courses`
--
ALTER TABLE `person_courses`
  ADD KEY `student_id` (`student_id`),
  ADD KEY `course_id` (`course_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `grade`
--
ALTER TABLE `grade`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `persons`
--
ALTER TABLE `persons`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `course_related`
--
ALTER TABLE `course_related`
  ADD CONSTRAINT `course_related_ibfk_1` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `course_related_ibfk_2` FOREIGN KEY (`related_course_id`) REFERENCES `courses` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `grade`
--
ALTER TABLE `grade`
  ADD CONSTRAINT `grade_ibfk_1` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `grade_ibfk_2` FOREIGN KEY (`student_id`) REFERENCES `persons` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `person_courses`
--
ALTER TABLE `person_courses`
  ADD CONSTRAINT `person_courses_ibfk_1` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `person_courses_ibfk_2` FOREIGN KEY (`student_id`) REFERENCES `persons` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
