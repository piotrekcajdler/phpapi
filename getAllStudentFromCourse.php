<?php
        header("Content-Type: application/json; charset=UTF-8");
        header('Access-Control-Allow-Origin: http://localhost:3000');
        header('Access-Control-Allow-Methods: GET, POST');
        header("Access-Control-Allow-Credentials: true");
        header("Access-Control-Allow-Headers: Content-Type");
    include_once 'person.php';
    include_once 'course.php';
    include_once 'grade.php';
    include_once 'connection.php';
  
    if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    $database = new Database();
    $db = $database->getConnection();
    $items = new Person($db);
    $cid = isset($_GET['cid']) ? $_GET['cid'] : die();
    @$stmt = $items->getAllStudentsFromCourse($cid);
    $itemCount = $stmt->rowCount();

    if($itemCount > 0){
        $people = array();
        $people["body"] = array();
        $people["itemCount"] = $itemCount;
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
            extract($row);
            $e = array(
                "id" => $id,
                "name" => $name,
                "lastname" => $lastname,
                "isActive" => $isActive,
                "email" => $email,
                "universityID" => $universityID,
                "phoneNumber" => $phoneNumber,
                "role" => $role,
                "degree" => $degree,
                "note" => $note,
                "Created" => $Created
            );
            array_push($people["body"], $e);
        }
        echo json_encode($people);
    }
    else{
        http_response_code(404);
        echo json_encode(
            array("message" => "No record found.")
        );
    }
}
else{
    http_response_code(400);
    echo json_encode("INVALID METHOD");
}
?>