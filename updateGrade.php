<?php
  header("Content-Type: application/json; charset=UTF-8");
  header('Access-Control-Allow-Origin: http://localhost:3000');
  header('Access-Control-Allow-Methods: GET, POST,DELETE,PATCH');
  header("Access-Control-Allow-Credentials: true");
  header("Access-Control-Allow-Headers: Content-Type");
  header("HTTP/1.1 200 OK");
    
   include_once 'person.php'; 
   include_once 'course.php'; 
   include_once 'grade.php'; 
   include_once 'connection.php';
  
    if ($_SERVER['REQUEST_METHOD'] === 'PATCH') {
        $database = new Database();
        $db = $database->getConnection();
        $item = new Grade($db);

        $data = json_decode(file_get_contents("php://input"));

        $nP = new Grade($db);
        $nP->id = $data->id;
        $nP->getGrade();
        if ($nP-> grade === null) {
            http_response_code(400);
            echo json_encode("Data could not be updated, Grade does not exist");
            return false;
    
        }
        if ($nP->isSubmitted === 1) {
            http_response_code(400);
            echo json_encode("Submitted grade cannot be changed");
            return false;
    
        }

        if (!((1 <= $data->grade) && ($data->grade <= 10))) {
            http_response_code(400);
            echo json_encode("Cannot create, grade is in wrong scale");
            return false;
        }


        $item->id= $data->id;
        $item->grade= $data->grade;
        $item->note = $data->note;
        
        if($item->updateGrade()){
            echo json_encode("Grade Updated.");
        } else{
            http_response_code(400);
            echo json_encode("Cannot create.");
        }
   }
   else{
    http_response_code(400);
    echo json_encode("INVALID METHOD");
}
    ?>