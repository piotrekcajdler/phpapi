<?php
      header("Content-Type: application/json; charset=UTF-8");
      header('Access-Control-Allow-Origin: http://localhost:3000');
      header('Access-Control-Allow-Methods: GET, POST,DELETE,PATCH');
      header("Access-Control-Allow-Credentials: true");
      header("Access-Control-Allow-Headers: Content-Type");
    include_once 'course.php'; 
    include_once 'connection.php';
    include_once 'person.php';
    if ($_SERVER['REQUEST_METHOD'] === 'POST') { 
    
        $database = new Database();
        $db = $database->getConnection();
        $item = new Course($db);
        $newC = new Course($db);
        $prof = new Person($db);
        $item->id = isset($_GET['cid']) ? $_GET['cid'] : die();
        $tid =  isset($_GET['tid']) ? $_GET['tid'] : die();

        $courseIfPersist = new Course($db);
        $courseIfPersist->id = $item->id;
        $courseIfPersist->getCourse();

        $prof->id = $tid;
        $prof->getSingleEmployee();
        if ($prof->name === null) {
            http_response_code(400);
            echo json_encode("Cannot create, teacher does not exist in database");
            return false;
        }

        if ($courseIfPersist->name === null  ){
            http_response_code(404);
            echo json_encode("Course does not exist");
            return false;
        }

        if ($courseIfPersist->teacher_id !==0 ){
            http_response_code(400);
            echo json_encode("Cannot bind, course  has already been binded to teacher");
            return false;
        }

        @$newC->getCourseForProfessor($tid);

        if ($newC->id != null ){
            http_response_code(400);
            echo json_encode("Cannot create, teacher has been already assigned");
            return false;
        }

        if( @$item->bindTeacherFromCourse($item->id,$tid)){
            echo json_encode("Teacher bined.");
            return true;
        } 
    }   else{
        http_response_code(400);
        echo json_encode("INVALID METHOD");
    }

?>