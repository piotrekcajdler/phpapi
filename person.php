<?php
    class Person{
        // Connection
        private $conn;
        // Tables
        private $db_table = "persons";
        // Columns
        public $id;
        public $name;
        public $email;
        public $lastname;
        public $password;
        public $phoneNumber;
        public $isActive;
        public $role;
        public $universityID;
        public $Created;
        public $note;
        // Db connection
        public function __construct($db){
            $this->conn = $db;
        }
        // GET ALL
        public function getEmployees(){
            $sqlQuery = "SELECT * FROM " . $this->db_table . " WHERE isActive=1";
            $stmt = $this->conn->prepare($sqlQuery);
            $stmt->execute();
            return $stmt;
        }

        public function getAllStudentsFromCourse($course_id){
            $sqlQuery = "SELECT  persons.id, persons.name, email, lastname, phoneNumber,persons.isActive,role,universityID,persons.note,persons.Created FROM  persons LEFT OUTER JOIN person_courses  
            ON persons.ID=person_courses.student_id WHERE  persons.isActive=1 AND person_courses.course_id = :course_id AND persons.role='student'  UNION SELECT  persons.id, persons.name, email, lastname, phoneNumber,persons.isActive,role,universityID,persons.note,persons.Created FROM  persons RIGHT OUTER JOIN person_courses  
            ON persons.ID=person_courses.student_id  WHERE  persons.isActive=1 AND person_courses.course_id = :course_id AND persons.role ='student' ";
            
            $stmt = $this->conn->prepare($sqlQuery);
            $stmt->bindParam(":course_id", htmlspecialchars(strip_tags($course_id)));
            $stmt->execute();
            return $stmt;
        }

        public function getAllFreeTeachers(){
            $sqlQuery = "SELECT * From persons WHERE persons.id NOT IN (SELECT  persons.id FROM  persons LEFT OUTER JOIN courses  ON persons.id=courses.teacher_id WHERE persons.id = courses.teacher_id AND persons.isActive=1
            UNION
            SELECT  persons.id FROM  persons RIGHT OUTER JOIN courses  ON persons.id=courses.teacher_id  WHERE persons.id = courses.teacher_id AND persons.isActive=1) AND persons.role='teacher'";
            $stmt = $this->conn->prepare($sqlQuery);
            $stmt->execute();
            return $stmt;
        }

        public function checkIfStudentIsEnrolled($sid,$cid){

            $sqlQuery = "SELECT 1 FROM person_courses WHERE student_id = :sid AND course_id = :cid";
            $stmt = $this->conn->prepare($sqlQuery);
            $stmt->bindParam(":sid", htmlspecialchars(strip_tags($sid)));
            $stmt->bindParam(":cid", htmlspecialchars(strip_tags($cid)));
            $stmt->execute();
            return $stmt;

        }

        public function getProfessorFromCourse($course_id){
            $sqlQuery = "SELECT  persons.id, persons.name, email, lastname, phoneNumber,persons.isActive,role,universityID,persons.note,persons.Created FROM  persons LEFT OUTER JOIN courses  ON persons.id=courses.teacher_id WHERE persons.id = courses.teacher_id AND persons.isActive=1 AND courses.id = :course_id
            UNION
            SELECT  persons.id, persons.name, email, lastname, phoneNumber,persons.isActive,role,universityID,persons.note,persons.Created FROM  persons RIGHT OUTER JOIN courses  ON persons.id=courses.teacher_id  WHERE persons.id = courses.teacher_id AND persons.isActive=1 AND courses.id = :course_id";
            $stmt = $this->conn->prepare($sqlQuery);
            $stmt->bindParam(":course_id", htmlspecialchars(strip_tags($course_id)));
            $stmt->execute();
            $dataRow = $stmt->fetch(PDO::FETCH_ASSOC);
            if (!empty($dataRow['name'])){
                $this->id = $dataRow['id'];
                $this->name = $dataRow['name'];
                $this->lastname = $dataRow['lastname'];
                $this->phoneNumber = $dataRow['phoneNumber'];
                $this->email = $dataRow['email'];
                $this->isActive = $dataRow['isActive'];
                $this->role = $dataRow['role'];
                $this->universityID = $dataRow['universityID'];
                $this->note = $dataRow['note'];
                $this->Created = $dataRow['Created'];
            }
        }

        // CREATE
        public function createEmployee(){
            $sqlQuery = "INSERT INTO
                        ". $this->db_table ."
                    SET
                        name = :name, 
                        lastname = :lastname,
                        email = :email,
                        password = :password, 
                        phoneNumber = :phoneNumber,
                        isActive = true, 
                        role = :role, 
                        universityID = :universityID,
                        note = :note,
                        Created = NOW()
                        ";
        
            $stmt = $this->conn->prepare($sqlQuery);
        
            // sanitize
            $this->name=htmlspecialchars(strip_tags($this->name));
            $this->email=htmlspecialchars(strip_tags($this->email));
            $this->password=htmlspecialchars(strip_tags($this->password));
            $this->lastname=htmlspecialchars(strip_tags($this->lastname));
            $this->phoneNumber=htmlspecialchars(strip_tags($this->phoneNumber));
            $this->role=htmlspecialchars(strip_tags($this->role));
            $this->universityID=htmlspecialchars(strip_tags($this->universityID));
            $this->note=htmlspecialchars(strip_tags($this->note));
        
            // bind data
            $stmt->bindParam(":name", $this->name);
            $stmt->bindParam(":email", $this->email);
            $stmt->bindParam(":lastname", $this->lastname);
            $stmt->bindParam(":phoneNumber", $this->phoneNumber);
            $stmt->bindParam(":password", $this->password);
            $stmt->bindParam(":role", $this->role);
            $stmt->bindParam(":universityID", $this->universityID);
            $stmt->bindParam(":note", $this->note);
        
            if($stmt->execute()){
               return true;
            }
            return false;
        }


        
        // READ single
        public function getSingleEmployee(){
            $sqlQuery = "SELECT * FROM " . $this->db_table . " WHERE isActive=1 AND id = ?";

            $stmt = $this->conn->prepare($sqlQuery);
            $stmt->bindParam(1, $this->id);
            $stmt->execute();
            $dataRow = $stmt->fetch(PDO::FETCH_ASSOC);
            if (!empty($dataRow['name'])){
                $this->id = $dataRow['id'];
                $this->name = $dataRow['name'];
                $this->lastname = $dataRow['lastname'];
                $this->phoneNumber = $dataRow['phoneNumber'];
                $this->email = $dataRow['email'];
                $this->isActive = $dataRow['isActive'];
                $this->role = $dataRow['role'];
                $this->universityID = $dataRow['universityID'];
                $this->note = $dataRow['note'];
                $this->Created = $dataRow['Created'];
            }
        }        


        public function createHistory($teacher_id, $course_id, $semester, $year){
            $sqlQuery = "INSERT INTO
                       teacher_courses_history
                    SET
                        teacher_id = :teacher_id, 
                        course_id =   :course_id,
                        year = :year,
                        semester= :semester,
                        Created = NOW()
                        ";
        
            $stmt = $this->conn->prepare($sqlQuery);
        

            // bind data
            $stmt->bindParam(":teacher_id",htmlspecialchars(strip_tags($teacher_id)));
            $stmt->bindParam(":course_id",htmlspecialchars(strip_tags($course_id)));
            $stmt->bindParam(":semester",htmlspecialchars(strip_tags($semester)));
            $stmt->bindParam(":year",htmlspecialchars(strip_tags($year)));
            
        
            if($stmt->execute()){
               return true;
            }
            return false;
        }
        public function login(){
            $sqlQuery = "SELECT * FROM " . $this->db_table . " WHERE isActive=1 AND email=:email AND password=:password";

            $stmt = $this->conn->prepare($sqlQuery);
            $stmt->bindParam(":email", htmlspecialchars(strip_tags($this->email)));
            $stmt->bindParam(":password", htmlspecialchars(strip_tags($this->password)));
            $stmt->execute();
            $dataRow = $stmt->fetch(PDO::FETCH_ASSOC);
            if (!empty($dataRow['name'])){
                $this->id = $dataRow['id'];
                $this->name = $dataRow['name'];
                $this->lastname = $dataRow['lastname'];
                $this->phoneNumber = $dataRow['phoneNumber'];
                $this->email = $dataRow['email'];
                $this->isActive = $dataRow['isActive'];
                $this->role = $dataRow['role'];
                $this->universityID = $dataRow['universityID'];
                $this->degree = $dataRow['degree'];
                $this->note = $dataRow['note'];
                $this->Created = $dataRow['Created'];
            }
        }        
        // UPDATE
        public function updateEmployee(){
            $sqlQuery = "UPDATE
                        ". $this->db_table ."
                    SET
                        name = :name, 
                        lastname = :lastname,
                        email = :email,
                        phoneNumber = :phoneNumber,
                        role = :role, 
                        universityID = :universityID,
                        note = :note
                    WHERE 
                        id = :id";
        
            $stmt = $this->conn->prepare($sqlQuery);
        
            // sanitize
            $this->name=htmlspecialchars(strip_tags($this->name));
            $this->email=htmlspecialchars(strip_tags($this->email));
            $this->id=htmlspecialchars(strip_tags($this->id));
            $this->lastname=htmlspecialchars(strip_tags($this->lastname));
            $this->phoneNumber=htmlspecialchars(strip_tags($this->phoneNumber));
            $this->role=htmlspecialchars(strip_tags($this->role));
            $this->universityID=htmlspecialchars(strip_tags($this->universityID));
            $this->note=htmlspecialchars(strip_tags($this->note));
        
            // bind data
            $stmt->bindParam(":id", $this->id);
            $stmt->bindParam(":name", $this->name);
            $stmt->bindParam(":email", $this->email);
            $stmt->bindParam(":lastname", $this->lastname);
            $stmt->bindParam(":phoneNumber", $this->phoneNumber);
            $stmt->bindParam(":role", $this->role);
            $stmt->bindParam(":universityID", $this->universityID);
            $stmt->bindParam(":note", $this->note);
        
            if($stmt->execute()){
               return true;
            }
            return false;
        }
        // DELETE
        function deleteEmployee(){
            $sqlQuery = "UPDATE
            ". $this->db_table ."
        SET
            isActive = 0
        WHERE 
            id = ?";

            $stmt = $this->conn->prepare($sqlQuery);
            $this->id=htmlspecialchars(strip_tags($this->id));
            $stmt->bindParam(1, $this->id);
            if($stmt->execute()){
                return true;
            }
            return false;
        }
    }
?>