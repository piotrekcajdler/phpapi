<?php
  header("Content-Type: application/json; charset=UTF-8");
  header('Access-Control-Allow-Origin: http://localhost:3000');
  header('Access-Control-Allow-Methods: GET, POST,DELETE,PATCH');
  header("Access-Control-Allow-Credentials: true");
  header("Access-Control-Allow-Headers: Content-Type");
  header("HTTP/1.1 200 OK");
    
   include_once 'person.php'; 
   include_once 'connection.php';
  
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $database = new Database();
        $db = $database->getConnection();
        $item = new Person($db);
        $data = json_decode(file_get_contents("php://input"));
        $item->name= $data->name;
        $item->lastname=$data->lastname;
        $item->email = $data->email;
        $item->password = $data->password;
        $item->universityID = $data->universityID;
        $item->phoneNumber=$data->phoneNumber;
        $item->degree = $data->degree;
        $item->role = $data->role;
        $item->note = $data->note;
        
        if($item->createEmployee()){
            echo json_encode("Created.");
        } else{
            http_response_code(400);
            echo json_encode("Cannot create");
        }
   }
   else{
    http_response_code(400);
    echo json_encode("INVALID METHOD");
}
  
    
    ?>