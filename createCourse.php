<?php
  header("Content-Type: application/json; charset=UTF-8");
  header('Access-Control-Allow-Origin: http://localhost:3000');
  header('Access-Control-Allow-Methods: GET, POST,DELETE,PATCH');
  header("Access-Control-Allow-Credentials: true");
  header("HTTP/1.1 200 OK");
  header("Access-Control-Allow-Headers: Content-Type");
    
   include_once 'person.php'; 
   include_once 'course.php'; 
   include_once 'connection.php';
  
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $database = new Database();
        $db = $database->getConnection();
        $item = new Course($db);
        $prof = new Person($db);
        $newC = new Course($db);
        

        $data = json_decode(file_get_contents("php://input"));
        $prof->id = $data->teacher_id;
        $prof->getSingleEmployee();
        if ($prof->name === null) {
            http_response_code(400);
            echo json_encode("Cannot create, teacher does not exist in database");
            return false;
        }
        
        @$newC->getCourseForProfessor($prof->id);

        if ($newC->id != null ){
            http_response_code(400);
            echo json_encode("Cannot create, teacher has been already assigned");
            return false;
        }



        $item->name= $data->name;
        $item->theory_grade_per=$data->theory_grade_per;
        $item->lab_grade_per = $data->lab_grade_per;
        $item->teacher_id = $data->teacher_id;
        $item->year = $data->year;
        $item->semester = $data->semester;
        $item->note = $data->note;
        
        if($item->createCourse()){
            $created = new Course($db);
            @$created->getCourseForProfessor($data->teacher_id);
            @$prof->createHistory($data->teacher_id,$created->id,$data->semester,$data->year);
            echo json_encode("Created");
        } else{
            http_response_code(400);
            echo json_encode("Cannot create");
        }
   }
   else{
    http_response_code(400);
    echo json_encode("INVALID METHOD");
}
  
    
    ?>