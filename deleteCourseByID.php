<?php
   header("Content-Type: application/json; charset=UTF-8");
   header('Access-Control-Allow-Origin: http://localhost:3000');
   header('Access-Control-Allow-Methods: GET, POST,DELETE,PATCH');
   header("Access-Control-Allow-Credentials: true");
   header("Access-Control-Allow-Headers: Content-Type");
   header("HTTP/1.1 200 OK");
    include_once 'course.php'; 
    include_once 'connection.php';
    if ($_SERVER['REQUEST_METHOD'] === 'DELETE') { 
    
        $database = new Database();
        $db = $database->getConnection();
        $item = new Course($db);
        $item->id = isset($_GET['id']) ? $_GET['id'] : die();

        $courseIfPersist = new Course($db);
        $courseIfPersist->id = $item->id;
        $courseIfPersist->getCourse();

        if($courseIfPersist->name != null  && $item->deleteCourse()){
            echo json_encode("Course deleted.");
        } else{
            http_response_code(404);
            echo json_encode("Data could not be deleted");
        }
    }   else{
        http_response_code(400);
        echo json_encode("INVALID METHOD");
    }

?>