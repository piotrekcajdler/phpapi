<?php
    header("Content-Type: application/json; charset=UTF-8");
    header('Access-Control-Allow-Origin: http://localhost:3000');
    header('Access-Control-Allow-Methods: GET, POST,DELETE,PATCH');
    header("Access-Control-Allow-Credentials: true");
    header("Access-Control-Allow-Headers: Content-Type");
    header("HTTP/1.1 200 OK");
    
   include_once 'person.php'; 
   include_once 'course.php'; 
   include_once 'connection.php';
  
   if ($_SERVER['REQUEST_METHOD'] === 'PATCH') {
    $database = new Database();
    $db = $database->getConnection();
    $item = new Course($db);
    $prof = new Person($db);

    $data = json_decode(file_get_contents("php://input"));
    $prof->id = $data->teacher_id;
    $prof->getSingleEmployee();
    if ($prof->name === null) {
        http_response_code(400);
        echo json_encode("Cannot update, teacher does not exist in database");
        return false;
    }

    $nC = new Course($db);
    $nC->id = $data->id;
    $nC->getCourse();
    if ($nC-> name === null) {
        http_response_code(400);
        echo json_encode("Data could not be updated, Course does not exist");
        return false;

    }


    $item->id= $data->id;
    $item->name= $data->name;
    $item->theory_grade_per=$data->theory_grade_per;
    $item->lab_grade_per = $data->lab_grade_per;
    $item->semester=$data->semester;
    $item->year = $data->year;
    $item->teacher_id = $data->teacher_id;
    $item->note = $data->note;

    if($item->updateCourse()){
        echo json_encode("Course data updated.");
    } else{
        http_response_code(400);
        echo json_encode("Data could not be updated");
    }}else{
        http_response_code(400);
        echo json_encode("INVALID METHOD");
    }
    
?>