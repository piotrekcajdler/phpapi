<?php
   header("Content-Type: application/json; charset=UTF-8");
   header('Access-Control-Allow-Origin: http://localhost:3000');
   header('Access-Control-Allow-Methods: GET, POST,DELETE,PATCH');
   header("Access-Control-Allow-Credentials: true");
   header("Access-Control-Allow-Headers: Content-Type");
    
   include_once 'course.php'; 
   include_once 'connection.php';
   if ($_SERVER['REQUEST_METHOD'] === 'GET') {

    $database = new Database();
    $db = $database->getConnection();
    $item = new Course($db);
    $item->id = isset($_GET['id']) ? $_GET['id'] : die();
    
    $item->getCourse();
    if($item->name != null){
        $e = array(
            "id" => $item->id,
            "name" => $item->name,
            "isActive" => $item->isActive,
            "teacher_id" => $item->teacher_id,
            "lab_grade_per" => $item->lab_grade_per,
            "semester" => $semester,
            "year" => $year,
            "theory_grade_per" => $item->theory_grade_per,
            "note" => $item->note,
            "Created"=>$item->Created,
        );
      
        http_response_code(200);
        echo json_encode($e);
    }
    else{
        http_response_code(404);
        echo json_encode("NOT FOUND");
    }}else{
        http_response_code(400);
        echo json_encode("INVALID METHOD");
    }
?>

