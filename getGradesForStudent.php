<?php
 header("Content-Type: application/json; charset=UTF-8");
 header('Access-Control-Allow-Origin: http://localhost:3000');
 header('Access-Control-Allow-Methods: GET, POST,DELETE,PATCH');
 header("Access-Control-Allow-Credentials: true");
 header("Access-Control-Allow-Headers: Content-Type");
    include_once 'person.php';
    include_once 'course.php';
    include_once 'grade.php';
    include_once 'connection.php';
  
    if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    $database = new Database();
    $db = $database->getConnection();
    $items = new Grade($db);
    $sid = isset($_GET['sid']) ? $_GET['sid'] : die();
    @$stmt = $items->getAllGradesForStudent($sid);
    $itemCount = $stmt->rowCount();

    if($itemCount > 0){
        $gradeArr = array();
        $gradeArr["body"] = array();
        $gradeArr["itemCount"] = $itemCount;
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
            extract($row);
            $e = array(
                "id" => $id,
                "grade" => $grade,
                "student_id" => $student_id,
                "course_id" => $course_id,
                "isActive" => $isActive,
                "type" => $type,
                "isSubmitted" => $isSubmitted,
                "note" => $note,
                "Created" => $Created
            );
            array_push($gradeArr["body"], $e);
        }
        echo json_encode($gradeArr);
    }
    else{
        http_response_code(404);
        echo json_encode(
            array("message" => "No record found.")
        );
    }
}
else{
    http_response_code(400);
    echo json_encode("INVALID METHOD");
}
?>