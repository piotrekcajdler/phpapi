<?php
 header("Content-Type: application/json; charset=UTF-8");
 header('Access-Control-Allow-Origin: http://localhost:3000');
 header('Access-Control-Allow-Methods: GET, POST,DELETE,PATCH');
 header("Access-Control-Allow-Credentials: true");
 header("HTTP/1.1 200 OK");
 header("Access-Control-Allow-Headers: Content-Type");
   include_once 'person.php'; 
   include_once 'course.php'; 
   include_once 'grade.php'; 
   include_once 'connection.php';
  
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $database = new Database();
        $db = $database->getConnection();
        $item = new Grade($db);
        $std = new Person($db);
        $course = new Course($db);
        $isFinal = isset($_GET['isfin']) ? $_GET['isfin'] : die();

        $data = json_decode(file_get_contents("php://input"));
        $std->id = $data->student_id;
        @$std->getSingleEmployee();
        if ($std->name === null) {
            http_response_code(400);
            echo json_encode("Cannot create, student does not exist in database");
            return false;
        }

        $course->id = $data->course_id;
        @$course->getCourse();
        if ($course->name === null) {
            http_response_code(400);
            echo json_encode("Cannot create, course does not exist in database");
            return false;
        }

        if (!((1 <= $data->grade) && ($data->grade <= 10))) {
            http_response_code(400);
            echo json_encode("Cannot create, grade is in wrong scale");
            return false;
        }
        
        $item->grade= $data->grade;
        $item->student_id=$data->student_id;
        $item->course_id = $data->course_id;
        $item->note = $data->note;
        $item->type = $data->type;
        
        if(@$item->createGrade($isFinal)){
            echo json_encode("Created.");
        } else{
            http_response_code(400);
            echo json_encode("Cannot create.");
        }
   }
   else{
    http_response_code(400);
    echo json_encode("INVALID METHOD");
}
  
    
    ?>