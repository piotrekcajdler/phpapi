<?php
 header("Content-Type: application/json; charset=UTF-8");
 header('Access-Control-Allow-Origin: http://localhost:3000');
 header('Access-Control-Allow-Methods: GET, POST,DELETE,PATCH');
 header("Access-Control-Allow-Credentials: true");
 header("HTTP/1.1 200 OK");
 header("Access-Control-Allow-Headers: Content-Type");
    
   include_once 'person.php'; 
   include_once 'course.php'; 
   include_once 'grade.php'; 
   include_once 'connection.php';
  
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $database = new Database();
        $db = $database->getConnection();
        $std = new Course($db);
        $course = new Course($db);
       

        $data = json_decode(file_get_contents("php://input"));
        $std->id = $data->related_course_id;
        @$std->getCourse();
        if ($std->name === null) {
            http_response_code(400);
            echo json_encode("Cannot delete, Course does not exist in database");
            return false;
        }

        $course->id = $data->course_id;
        @$course->getCourse();
        if ($course->name === null) {
            http_response_code(400);
            echo json_encode("Cannot delete, course does not exist in database");
            return false;
        }

        if (@!$course->checkIfRelatedAlready($std->id)) {
            http_response_code(400);
            echo json_encode("Cannot delete, course is not associated");
            return false;
        }
        

        
        if(@$course->removeRelatedCourse($data->course_id,$data->related_course_id)){
            echo json_encode("Deleted successfully.");
        } else{
            http_response_code(400);
            echo json_encode("Cannot delete.");
        }
   }
   else{
    http_response_code(400);
    echo json_encode("INVALID METHOD");
}
  
    
    ?>