<?php
    class Course{
        // Connection
        private $conn;
        // Table
        private $db_table = "courses";
        // Columns
        public $id;
        public $name;
        public $teacher_id;
        public $theory_grade_per;
        public $lab_grade_per;
        public $isActive;
        public $note;
        public $year;
        public $semester;
        public $Created;
        // Db connection
        public function __construct($db){
            $this->conn = $db;
        }
        // GET ALL
        public function getAllCourses(){
            $sqlQuery = "SELECT id, name, teacher_id, theory_grade_per, lab_grade_per,isActive,note,Created,year,semester FROM " . $this->db_table . " WHERE isActive=1";
            $stmt = $this->conn->prepare($sqlQuery);
            $stmt->execute();
            return $stmt;
        }

        public function getAllCoursesRelated($course_id){
            $sqlQuery = "SELECT id, name, teacher_id, theory_grade_per, lab_grade_per,isActive,courses.note,courses.Created,year,semester FROM courses LEFT OUTER JOIN course_related ON courses.id=course_related.related_course_id WHERE courses.isActive=1 AND course_related.course_id = :course_id UNION SELECT id, name, teacher_id, theory_grade_per, lab_grade_per,isActive,courses.note,courses.Created,year,semester FROM courses RIGHT OUTER JOIN course_related ON courses.id=course_related.related_course_id WHERE courses.isActive=1 AND course_related.course_id = :course_id";
            $stmt = $this->conn->prepare($sqlQuery);
            $stmt->bindParam(":course_id", htmlspecialchars(strip_tags($course_id)));
            $stmt->execute();
            return $stmt;
        }

        public function getCourseForProfessor($tid){
            $sqlQuery = "SELECT id, name, teacher_id, theory_grade_per, lab_grade_per,isActive,note,year,semester FROM " . $this->db_table . " WHERE isActive=1 AND teacher_id = :tid";
            $stmt = $this->conn->prepare($sqlQuery);
            $stmt->bindParam(":tid", htmlspecialchars(strip_tags($tid)));
            $stmt->execute();
            $dataRow = $stmt->fetch(PDO::FETCH_ASSOC);
            if (!empty($dataRow['name'])){
            $this->name = $dataRow['name'];
            $this->id = $dataRow['id'];
            $this->teacher_id = $dataRow['teacher_id'];
            $this->theory_grade_per = $dataRow['theory_grade_per'];
            $this->lab_grade_per = $dataRow['lab_grade_per'];
            $this->isActive = $dataRow['isActive'];
            $this->Created = $dataRow['Created'];
            $this->note = $dataRow['note'];}
        }



        // CREATE
        public function createCourse(){
            $sqlQuery = "INSERT INTO
                        ". $this->db_table ."
                    SET
                        name = :name, 
                        teacher_id = :teacher_id,
                        theory_grade_per = :theory_grade_per, 
                        lab_grade_per = :lab_grade_per,
                        isActive = true,
                        year = :year,
                        semester = :semester,
                        note = :note,
                        Created= NOW()
                        ";
        
            $stmt = $this->conn->prepare($sqlQuery);
        
            // sanitize
            $this->name=htmlspecialchars(strip_tags($this->name));
            $this->teacher_id=htmlspecialchars(strip_tags($this->teacher_id));
            $this->theory_grade_per=htmlspecialchars(strip_tags($this->theory_grade_per));
            $this->lab_grade_per=htmlspecialchars(strip_tags($this->lab_grade_per));
            $this->note=htmlspecialchars(strip_tags($this->note));
            $this->year=htmlspecialchars(strip_tags($this->year));
            $this->semester=htmlspecialchars(strip_tags($this->semester));
        
            // bind data
            $stmt->bindParam(":name", $this->name);
            $stmt->bindParam(":teacher_id", $this->teacher_id);
            $stmt->bindParam(":theory_grade_per", $this->theory_grade_per);
            $stmt->bindParam(":lab_grade_per", $this->lab_grade_per);
            $stmt->bindParam(":note", $this->note);
            $stmt->bindParam(":year", $this->year);
            $stmt->bindParam(":semester", $this->semester);
        
            if($stmt->execute()){
               return true;
            }
            return false;
        }
        // READ single
        public function getCourse(){
            $sqlQuery = "SELECT id, name, teacher_id, theory_grade_per, lab_grade_per,isActive,note,Created,year,semester FROM " . $this->db_table . " WHERE isActive=1 AND id = ?";

            $stmt = $this->conn->prepare($sqlQuery);
            $stmt->bindParam(1, $this->id);
            $stmt->execute();
            $dataRow = $stmt->fetch(PDO::FETCH_ASSOC);
            if (!empty($dataRow['name'])){
            $this->name = $dataRow['name'];
            $this->teacher_id = $dataRow['teacher_id'];
            $this->theory_grade_per = $dataRow['theory_grade_per'];
            $this->lab_grade_per = $dataRow['lab_grade_per'];
            $this->isActive = $dataRow['isActive'];
            $this->note = $dataRow['note'];
            $this->year = $dataRow['year'];
            $this->semester = $dataRow['semester'];
            $this->Created = $dataRow['Created'];}
        }        
        // UPDATE
        public function updateCourse(){
            $sqlQuery = "UPDATE
                        ". $this->db_table ."
                    SET
                        name = :name, 
                        teacher_id = :teacher_id,
                        theory_grade_per = :theory_grade_per, 
                        lab_grade_per = :lab_grade_per,
                        semester = :semester, 
                        year = :year,
                        lab_grade_per = :lab_grade_per,
                        note = :note
                    WHERE 
                        id = :id";
        
            $stmt = $this->conn->prepare($sqlQuery);
        
            // sanitize
            $this->id=htmlspecialchars(strip_tags($this->id));
            $this->name=htmlspecialchars(strip_tags($this->name));
            $this->teacher_id=htmlspecialchars(strip_tags($this->teacher_id));
            $this->theory_grade_per=htmlspecialchars(strip_tags($this->theory_grade_per));
            $this->lab_grade_per=htmlspecialchars(strip_tags($this->lab_grade_per));
            $this->note=htmlspecialchars(strip_tags($this->note));
            $this->lab_grade_per=htmlspecialchars(strip_tags($this->year));
            $this->semester=htmlspecialchars(strip_tags($this->semester));

        
            // bind data
            $stmt->bindParam(":id", $this->id);
            $stmt->bindParam(":name", $this->name);
            $stmt->bindParam(":teacher_id", $this->teacher_id);
            $stmt->bindParam(":theory_grade_per", $this->theory_grade_per);
            $stmt->bindParam(":lab_grade_per", $this->lab_grade_per);
            $stmt->bindParam(":note", $this->note);
            $stmt->bindParam(":year", $this->year);
            $stmt->bindParam(":semester", $this->semester);
        
            if($stmt->execute()){
               return true;
            }
            return false;
        }
        // DELETE
        function deleteCourse(){
            $sqlQuery = "UPDATE
            ". $this->db_table ."
        SET
            isActive = 0
        WHERE 
            id = ?";

            $stmt = $this->conn->prepare($sqlQuery);
            $this->id=htmlspecialchars(strip_tags($this->id));
            $stmt->bindParam(1, $this->id);
            if($stmt->execute()){
                return true;
            }
            return false;
        }


        function unBindTeacherFromCourse($cid){
            $sqlQuery = "UPDATE
            ". $this->db_table ."
        SET
            teacher_id = null
        WHERE 
            id = ?";

            $stmt = $this->conn->prepare($sqlQuery);
            $this->id=htmlspecialchars(strip_tags($cid));
            $stmt->bindParam(1, $this->id);
            if($stmt->execute()){
                return true;
            }
            return false;
        }
        function bindTeacherFromCourse($cid,$tid){
            $sqlQuery = "UPDATE
            ". $this->db_table ."
        SET
            teacher_id = :tid
        WHERE 
            id = :cid";

            $stmt = $this->conn->prepare($sqlQuery);
            $this->id=htmlspecialchars(strip_tags($cid));
            $stmt->bindParam(":cid", $this->id);
            $stmt->bindParam(":tid", strip_tags($tid));
            if($stmt->execute()){
                return true;
            }
            return false;
        }

        public function addStudentToCourse($sid,$cid,$note){
            $sqlQuery = "INSERT INTO
                person_courses
            SET
                student_id = :sid,
                course_id = :cid,
                note = :note,
                Created=NOW()
                ";


            $stmt = $this->conn->prepare($sqlQuery);
                    
            // sanitize and bind
            $stmt->bindParam(":sid",htmlspecialchars(strip_tags($sid)));
            $stmt->bindParam(":cid",htmlspecialchars(strip_tags($cid)));
            $stmt->bindParam(":note",htmlspecialchars(strip_tags($note)));

            if($stmt->execute()){
            return true;
            }
            return false;

        }

        public function removeStudentFromCourse($sid,$cid){
            $sqlQuery = "DELETE FROM
                person_courses
            WHERE
                student_id = :sid AND
                course_id = :cid
                ";


            $stmt = $this->conn->prepare($sqlQuery);
                    
            // sanitize and bind
            $stmt->bindParam(":sid",htmlspecialchars(strip_tags($sid)));
            $stmt->bindParam(":cid",htmlspecialchars(strip_tags($cid)));
    
            if($stmt->execute()){
            return true;
            }
            return false;

        }

        public function addRelatedCourse($cid,$rcid,$note){
            $sqlQuery = "INSERT INTO
                course_related
            SET
                related_course_id = :sid,
                course_id = :cid,
                note = :note
                ";


            $stmt = $this->conn->prepare($sqlQuery);
                    
            // sanitize and bind
            $stmt->bindParam(":sid",htmlspecialchars(strip_tags($rcid)));
            $stmt->bindParam(":cid",htmlspecialchars(strip_tags($cid)));
            $stmt->bindParam(":note",htmlspecialchars(strip_tags($note)));

            if($stmt->execute()){
            return true;
            }
            return false;
        }


        public function checkIfRelatedAlready($rcid){
            $sqlQuery = "SELECT COUNT(course_id) FROM course_related WHERE course_id = :cid AND related_course_id = :rcid";
            $stmt = $this->conn->prepare($sqlQuery);
            $stmt->bindParam(":rcid", htmlspecialchars(strip_tags($rcid)));
            $stmt->bindParam(":cid", htmlspecialchars(strip_tags($this->id)));
            $stmt->execute();
            $isValid = $stmt->fetch();
            //echo json_encode($isValid["0"]);
            return $isValid["0"] === 0 ? false : true;
        }


        public function removeRelatedCourse($cid,$rcid){
            $sqlQuery = "DELETE FROM
                 course_related
            WHERE
                related_course_id = :sid AND
                course_id = :cid
                ";
            $stmt = $this->conn->prepare($sqlQuery);
                    
            // sanitize and bind
            $stmt->bindParam(":sid",htmlspecialchars(strip_tags($rcid)));
            $stmt->bindParam(":cid",htmlspecialchars(strip_tags($cid)));
    
            if($stmt->execute()){
            return true;
            }
            return false;
        }
    }
?>
