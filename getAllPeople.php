<?php
    header("Content-Type: application/json; charset=UTF-8");
    header('Access-Control-Allow-Origin: http://localhost:3000');
    header('Access-Control-Allow-Methods: GET, POST');
    header("Access-Control-Allow-Credentials: true");
    header("Access-Control-Allow-Headers: Content-Type");
    include_once 'person.php';
    include_once 'connection.php';
  
    if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    $database = new Database();
    $db = $database->getConnection();
    $items = new Person($db);
    $stmt = $items->getEmployees();
    $itemCount = $stmt->rowCount();

    if($itemCount > 0){
        $employeeArr = array();
        $employeeArr["body"] = array();
        $employeeArr["itemCount"] = $itemCount;
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
            extract($row);
            $e = array(
                "id" => $id,
                "name" => $name,
                "lastname" => $lastname,
                "isActive" => $isActive,
                "email" => $email,
                "universityID" => $universityID,
                "phoneNumber" => $phoneNumber,
                "degree" => $degree,
                "role" => $role,
                "note" => $note,
                "Created" => $Created
            );
            array_push($employeeArr["body"], $e);
        }
        echo json_encode($employeeArr);
    }
    else{
        http_response_code(404);
        echo json_encode(
            array("message" => "No record found.")
        );
    }
}
else{
    http_response_code(400);
    echo json_encode("INVALID METHOD");
}
?>